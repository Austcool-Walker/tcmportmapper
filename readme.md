TCMPortMapper is an Obj-C/Cocoa NAT-Traversal framework that maps ports via UPNP and NAT-PMP.

http://www.codingmonkeys.de/images/PortMap128.png

TCMPortMapper.framework uses libnatpmp and miniupnpc to provide an easy to use Cocoa interface to automatically map ports to your networked application via UPNP and NAT-PMP.

Included is a sample application to map ports and a short illustration (an echo server) of using the framework within your own code.

This shows the BasicUsage .

For a more complete featureset: the public header.

The framework supports 10.4 and greater.

Port Map Product Page at http://www.codingmonkeys.de

Here is a ListOfAppsUsingTCMPortMapper - if you use it in your app, please enter the app.

If you are into Windows development you might want to look at the .net port of this library at http://code.google.com/p/dotnetportmapper/ .
